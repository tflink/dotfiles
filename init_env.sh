#!/usr/bin/bash

# set this base dir if you've checked code out to somewhere other than
# $HOME/dotfiles
BASEDIR=$HOME/dotfiles

# function to symlink dotfile if it does not already exist
symlink_dotfile () {
if [ ! -e $HOME/.$dotfilename ] && [ ! -h $HOME/.$dotfilename ] && [ ! -d $HOME/.$dotfilename ]
then
    echo "symlinking $dotfilename"
    ln -s $BASEDIR/$dotfilename $HOME/.$dotfilename
else
    echo "$dotfilename exists, skipping"
fi
}

## symlink dotfiles and dotdirs
symlink_dotfiles () {
    DOTFILES="screenrc gitconfig bashrc inputrc vimrc vim tmux.conf emacs"
    for dotfile in $DOTFILES
    do
        dotfilename=$dotfile
        symlink_dotfile
    done
}

## prepare git submodules
git_submodules () {
    echo "preparing git submodules"
    git submodule init
    git submodule update
}

do_help () {
    echo "this is help"
}

#Check the number of arguments. If none are passed, print help and exit.
NUMARGS=$#

if [ $NUMARGS -eq 0 ]; then
    do_help
fi

while getopts gdah FLAG; do
    case $FLAG in
        g)
            git_submodules
            ;;
        d)
            symlink_dotfiles
            ;;
        a)
            echo "doing all the things"
            git_submodules
            symlink_dotfiles
            ;;
        h)
            do_help
            ;;
        \?)
            echo "unrecognized input"
            do_help
            ;;
    esac
done
