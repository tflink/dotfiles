;; packages
(setq package-archives '(;;("gnu" . "https://elpa.gnu.org/packages/")
                         ("org" . "http://orgmode.org/elpa/")
;;                        ("marmalade" . "https://marmalade-repo.org/packages/")
                         ("melpa" . "https://stable.melpa.org/packages/")))
(package-initialize)

(add-to-list 'load-path "~/.emacs.d/undo-tree")
(add-to-list 'load-path "~/.emacs.d/evil")

;;(defun require-package (package)
;;  (setq-default highlight-tabs t)
;;  "Install given PACKAGE."
;;  (unless (package-installed-p package)
;;    (unless (assoc package package-archive-contents)
;;      (package-refresh-contents))
;;    (package-install package)))
;;
;;;; load and enable solarized
;;;; this requires that emacs-color-theme-solarized is in the themes dir - https://github.com/sellout/emacs-color-theme-solarized
;;(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/emacs-color-theme-solarized")
;;(set-frame-parameter nil 'background-mode 'dark)
;;(set-terminal-parameter nil 'background-mode 'dark)
;;(load-theme 'solarized t)
;;
;;;; disable the tool bar
;;(tool-bar-mode -1)
;;
;;;; enable whitespace minor mode
;;(require 'whitespace)
;;
;;;; make sure that TAB works for org-mode with evil-mode
;;(setq evil-want-C-i-jump nil)
;;
;;;; evil mode
(require 'evil)
(evil-mode 1)
;;
;;;; evil-tabs
(global-evil-tabs-mode t)
;;
;;;; org-mode and evil-org-mode
;;(require 'evil-leader)
;;(require 'evil-jumper)
;;(require 'evil-org)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages (quote (org org-evil org-pomodoro))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
