Dotfiles
========

Several people have asked me to make my vim and git configs public, so I'm actually
doing it now. This process will help me standardize my configs and make getting
new installs configured faster, as well :)

I've stolen most of this from various people's configs over the last couple years
but I wasn't smart enough to write down where I got them from and now I'm not
finding everything

I'd love to give more people credit for these configs, but that's hard when I
don't remember who to give credit to. If you recognize any of these files as
your own that you've posted and I haven't credited you - please let me know
so I can fix that.

Same note on the license - assuming my memory serves, everything is GPL-able.

Usage
-----

I've created a basic shell script so that all you should have to do is clone
this repo into $HOME and run::

  sh init_env.sh

That script will initialize git submodules and create symlinks to all the relavant
config dotfiles.


Git Config
----------

Pretty much stolen verbatim from `Gary Bernhardt <https://github.com/garybernhardt/dotfiles>`_

If you haven't already seen it, his `screencast on git workflow
<https://www.destroyallsoftware.com/screencasts/catalog/git-workflow>`_
is well worth a subscription to `destroy all software
<https://www.destroyallsoftware.com/screencasts>`_ by itself

bashrc, tmux config
-------------------

Mostly sourced from `Adam Miller <https://github.com/maxamillion/dotfiles>`_
